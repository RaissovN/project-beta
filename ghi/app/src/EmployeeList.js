import { useEffect, useState } from 'react';

function SalesPersonList() {
  const [salesPeople, setSalesPeople] = useState([]);

  const fetchData = async () => {
      const url = 'http://localhost:8090/api/salespeople/';

      const response = await fetch(url);
      if (response.ok){
        const data = await response.json();
        setSalesPeople(data.Sales_people);
      }
    }

  useEffect(() => {
    fetchData();
  }, []);
  return(
    <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {salesPeople?.map((person, index) => (
            <tr key={index}>
              <td>{person.employee_id}</td>
              <td>{person.first_name}</td>
              <td>{person.last_name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default SalesPersonList;

import React, {useState} from 'react';
import { useNavigate} from 'react-router-dom';

function TechnicianForm() {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [employeeId, setEmployeeId] = useState("");
    const [errorMessage, setErrorMessage] = useState("");

    const clearForm = () => {
        setFirstName("");
        setLastName("");
        setEmployeeId("");
    };

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    };
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    };
    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    };

    const navigate = useNavigate();

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            first_name: firstName,
            last_name: lastName,
            employee_id: employeeId,
        }
        console.log(data)
        const technicianURL = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                "Content-type": "application/json",
            },
        };

        const response = await fetch(technicianURL, fetchConfig);
        const responseData = await response.json();

        if (response.ok) {
            console.log(responseData);
            clearForm();
            navigate('/technicians');
        } else {
            setErrorMessage(responseData.message);
        }
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Technician</h1>
            <div className="text-danger">{errorMessage}</div>
            <form onSubmit={handleSubmit} id="create-technician-form">
              <div className="form-floating mb-3">
                <input value={firstName} onChange={handleFirstNameChange} placeholder="First name" required type="text" name="first_name" id="first_name" className="form-control"/>
                <label htmlFor="name">First name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={lastName} onChange={handleLastNameChange} placeholder="Last name" required type="text" name="last_name" id="last_name" className="form-control"/>
                <label htmlFor="last_name">Last name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={employeeId} onChange={handleEmployeeIdChange} placeholder="Employee id" required type="number" name="employee_id" id="employee_id" className="form-control"/>
                <label htmlFor="employee_id">Employee ID</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default TechnicianForm;
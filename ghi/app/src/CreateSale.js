import React, { useEffect, useState } from 'react';

function RegisterSale() {
  const [customer, setCustomer] = useState(0);
  const [salesperson, setSalesPerson] = useState('');
  const [automobileVin, setAutomobileVin] = useState('');
  const [price, setPrice] = useState(0);
  const [salesPeople, setSalesPeople] = useState([])
  const [customerInfo, setCustomerInfo] = useState([]);
  const [automobileInfo, setAutomobileInfo] = useState([]);



  const fetchData = async () => {
    const salesPersonUrl = 'http://localhost:8090/api/salespeople/';

    const response = await fetch(salesPersonUrl);
    if (response.ok){
      const data = await response.json();
      setSalesPeople(data.Sales_people);
    }

    const customerUrl = 'http://localhost:8090/api/customers/';

      const customerInfoResponse = await fetch(customerUrl);
      if (customerInfoResponse.ok){
        const customerData = await customerInfoResponse.json();
        setCustomerInfo(customerData.customer);
      }

    const automobileUrl = 'http://localhost:8100/api/automobiles/';

    const automobileInfoResponse = await fetch(automobileUrl);
    if (automobileInfoResponse.ok){
    const data = await automobileInfoResponse.json();
    setAutomobileInfo(data.autos);
    }
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.salesperson = parseInt(salesperson);
    data.customer = parseInt(customer);
    data.vin = automobileVin;
    data.price = parseInt(price)

    const makeCustomerURL = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(makeCustomerURL, fetchConfig);
    if (response.ok) {
      const knew = await response.json();
    }

    const markSoldURL = `http://localhost:8100/api/automobiles/${automobileVin}/`;
    const soldFetchConfig = {
        method: 'put',
        body: JSON.stringify({"sold": true}),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const soldResponse = await fetch(markSoldURL, soldFetchConfig);
    if (soldResponse.ok) {
      const soldknew = await soldResponse.json();
      window.location.href='http://localhost:3000/sales/'
    }
  };

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };

  const handleSalesPersonChange = (event) => {
    const value = event.target.value;
    setSalesPerson(value);
  };

  const handleAutomobileVinChange = (event) => {
    const value = event.target.value;
    setAutomobileVin(value);
  };

  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  };
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Sale</h1>
          <form onSubmit={handleSubmit} id="create-customer-form">
            <div className="mb-3">
              <select value={salesperson} onChange={handleSalesPersonChange} required name="saleperson" id="salesperson" className="form-select">
                <option value="">Select Salesperson </option>
                {salesPeople.map((employee, index) => {
                  return (
                    <option key={index} value={employee.employee_id}>
                      {employee.first_name + " " + employee.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select value={customer} onChange={handleCustomerChange} required name="customer" id="customer" className="form-select">
                <option value="">Select Customer </option>
                {customerInfo.map((customer, index) => {
                  return (
                    <option key={index} value={customer.id}>
                      {customer.first_name + " " + customer.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select value={automobileVin} onChange={handleAutomobileVinChange} required name="automobileVin" id="automobileVin" className="form-select">
                <option value="">Select VIN </option>
                {automobileInfo.filter(car => car.sold === false).map((car, index) => {
                  return (
                    <option key={index} value={car.vin}>
                      {car.vin}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input value={price} onChange={handlePriceChange} placeholder="Price" required type="text" name="price" id="price" className="form-control"/>
              <label htmlFor="lastName">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default RegisterSale;

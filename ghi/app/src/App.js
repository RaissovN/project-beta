import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import MainPage from './MainPage';
import TechnicianForm from './TechnicianForm';
import TechniciansList from './TechniciansList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import SalesPersonList from './EmployeeList';
import CustomerList from './CustomerList';
import NewCustomer from './CreateCustomer';
import SalesList from './ListSales';
import NewEmployee from './CreateEmployee';
import RegisterSale from './CreateSale';
import SalespersonHistory from './SalesHistory';
import ManufacturerList from './ListManufacturers';
import VehicleModelList from './ListVehicleModel';
import InventoryList from './ListInventory';
import NewAutomobile from './CreateAutomobile';
import CreateManufacturer from './CreateManufacturer';
import NewVehicleModel from './CreateVehicleModel';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<CreateManufacturer />} />
          </Route>
          <Route path="models">
            <Route index element={<VehicleModelList />} />
            <Route path="new" element={<NewVehicleModel />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<InventoryList />} />
            <Route path="new" element={<NewAutomobile />} />
          </Route>
          <Route path="sales">
            <Route path="" element={<SalesList/>}/>
            <Route path="new" element={<RegisterSale/>}/>
          </Route>
          <Route path="customers">
            <Route path="" element={<CustomerList/>}/>
            <Route path="new" element={<NewCustomer/>}/>
          </Route>
          <Route path="salespeople">
            <Route path="" element={<SalesPersonList/>}/>
            <Route path="new" element={<NewEmployee/>}/>
            <Route path="history" element={<SalespersonHistory/>}/>
          </Route>
          <Route path="technicians">
            <Route index element={<TechniciansList/>}/>
            <Route path="new" element={<TechnicianForm/>}/>
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentList/>}/>
            <Route path="new" element={<AppointmentForm/>}/>
            <Route path="history" element={<ServiceHistory/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

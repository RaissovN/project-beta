import {useEffect, useState} from 'react';

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [filterValue, setFilterValue] = useState("")

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);

        } else {
            console.error("Response invalid");
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const formatDateTime = (dateTimeString) => {

        const dateTime = new Date(dateTimeString);
        return dateTime.toLocaleString('en-US');
      };

    const handleFilterChange = (event) => {
        setFilterValue(event.target.value)
    }

    const getFilteredAppointments = () => {
        return appointments.filter(a => a.vin.toLowerCase().includes(filterValue.toLowerCase()))
    }


    return (
        <>
            <h1>Service History</h1>
            <input onChange={handleFilterChange} placeholder='Search by VIN...'/>
            <table className="table table-striped">
            <thead>
            <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date and Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
                {getFilteredAppointments().map(appointment => {
                return (
                <tr key={appointment.id}>
                    <td>{ appointment.vin }</td>
                    { appointment.vip ? <td>YES</td> : <td>NO</td> }
                    <td>{ appointment.customer }</td>
                    <td>{ formatDateTime(appointment.date_time) }</td>
                    <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                    <td>{ appointment.reason }</td>
                    <td>{ appointment.status }</td>
                </tr>
                );
            })}
            </tbody>
        </table>
        </>
    )
}

export default ServiceHistory;
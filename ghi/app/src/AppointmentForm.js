import React, {useState, useEffect} from 'react';
import { useNavigate} from 'react-router-dom';

function AppointmentForm() {
    const [technicians, setTechnicians] = useState([]);
    const [vin, setVin] = useState("");
    const [customer, setCustomer] = useState("");
    const [dateTime, setDateTime] = useState("");
    const [technician, setTechnician] = useState("");
    const [reason, setReason] = useState("");
    const [errorMessage, setErrorMessage] = useState("");

    const clearForm = () => {
        setVin("");
        setCustomer("");
        setDateTime("");
        setTechnician("");
        setReason("");
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    };
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    };
    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    };
    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    };
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    };

    const navigate = useNavigate();

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            vin: vin,
            customer: customer,
            date_time: dateTime,
            technician: technician,
            reason: reason,
        }
        console.log(data)
        const appointmentURL = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                "Content-type": "application/json",
            },
        };
        const response = await fetch(appointmentURL, fetchConfig);
        const responseData = await response.json();

        if (response.ok) {
            console.log(responseData);
            clearForm();
            navigate('/appointments');
        } else {
            setErrorMessage(responseData.message);
        }
    }

    const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setTechnicians(data.technicians);
    }

    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a service appointment</h1>
            <div className="text-danger">{errorMessage}</div>
            <form onSubmit={handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                <input value={vin} onChange={handleVinChange}
                    placeholder="Vin"
                    required
                    type="text"
                    name="vin"
                    id="vin"
                    className="form-control"
                />
                <label htmlFor="vin">Automobile VIN</label>
                </div>
                <div className="form-floating mb-3">
                <input value={customer}  onChange={handleCustomerChange}
                    placeholder="Customer"
                    required
                    type="text"
                    name="customer"
                    id="customer"
                    className="form-control"
                />
                <label htmlFor="customer">Customer</label>
                </div>
                <div className="form-floating mb-3">
                <input value={dateTime}  onChange={handleDateTimeChange}
                    placeholder="Date and time"
                    required
                    type="datetime-local"
                    name="date_time"
                    id="date_time"
                    className="form-control"
                />
                <label htmlFor="date_time">Date and Time</label>
                </div>
                <div className="mb-3">
                <select value={technician} onChange={handleTechnicianChange}
                    required
                    id="technician"
                    name="technician"
                    className="form-select"
                >
                    <option value="">Technician</option>
                    {technicians.map(technician => {
                        return (
                            <option key={technician.employee_id} value={technician.employee_id}>
                                {technician.first_name} {technician.last_name}
                            </option>
                        )
                    })}
                </select>
                </div>
                <div className="form-floating mb-3">
                <input value={reason}  onChange={handleReasonChange}
                    placeholder="Customer"
                    required
                    type="text"
                    name="reason"
                    id="reason"
                    className="form-control"
                />
                <label htmlFor="reason">Reason</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    )

}

export default AppointmentForm;
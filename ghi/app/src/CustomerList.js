import { useEffect, useState } from 'react';

function CustomerList() {
  const [customer, setCustomer] = useState([]);

  const fetchData = async () => {
      const url = 'http://localhost:8090/api/customers/';

      const response = await fetch(url);
      if (response.ok){
        const data = await response.json();
        setCustomer(data.customer);
      }
    }
  useEffect(() => {
    fetchData();
  }, []);
  return(
    <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {customer?.map((person, index) => (
            <tr key={index}>
              <td>{person.first_name}</td>
              <td>{person.last_name}</td>
              <td>{person.phone_number}</td>
              <td>{person.address}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default CustomerList;

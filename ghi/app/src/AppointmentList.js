import React, {useState, useEffect} from 'react';


function AppointmentList() {
    const [appointments, setAppointments] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);

        } else {
            console.error("Response invalid")
        }
    }

    const finish = async (event) => {
        const id = event.target.id;
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
            method: "PUT"
        });
        if (response.ok) {
            fetchData();
        }
    }

    const cancel = async (event) => {

        const id = event.target.id;
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
            method: "PUT"
        });
        if (response.ok) {
            fetchData();
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const formatDateTime = (dateTimeString) => {

        const dateTime = new Date(dateTimeString);
        return dateTime.toLocaleString('en-US');
      };


    return (
        <div>
            <h1>Service Appointments</h1>
            <table className="table table-striped">
            <thead>
            <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date and Time</th>
                <th>Technician</th>
                <th>Reason</th>
            </tr>
            </thead>
            <tbody>
                {appointments.filter((appointment) =>
                appointment.status !== "Canceled" &&
                appointment.status !== "Finished"
                )
                .map(appointment => {
                return (
                <tr key={appointment.id}>
                    <td>{ appointment.vin }</td>
                    { appointment.vip ? <td>YES</td> : <td>NO</td> }
                    <td>{ appointment.customer }</td>
                    <td>{ formatDateTime(appointment.date_time) }</td>
                    <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                    <td>{ appointment.reason }</td>
                    <td>
                        <button onClick={finish} id={appointment.id} type="finish-button" className="btn btn-success">Finish</button>
                    </td>
                    <td>
                        <button onClick={cancel} id={appointment.id} type="cancel-button" className="btn btn-danger">Cancel</button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
        </div>
    )
}

export default AppointmentList;
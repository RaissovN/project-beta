import { useEffect, useState } from 'react';

function SalesList() {
  const [sales, setSales] = useState([]);

  const fetchData = async () => {
      const url = 'http://localhost:8090/api/sales/';

      const response = await fetch(url);
      if (response.ok){
        const data = await response.json();
        setSales(data.sale);
      }
    }
  useEffect(() => {
    fetchData();
  }, []);
  return(
    <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>VIN</th>
            <th>Customer</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales?.map((sale, index) => (
            <tr key={index}>
              <td>{sale.salesperson.first_name + " " + sale.salesperson.last_name}</td>
              <td>{sale.vin.vin}</td>
              <td>{sale.customer.first_name + " " + sale.customer.last_name}</td>
              <td>${sale.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default SalesList;

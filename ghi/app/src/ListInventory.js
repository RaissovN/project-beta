import { useEffect, useState } from 'react';

function InventoryList() {
  const [inventory, setInventory] = useState([]);

  const fetchData = async () => {
      const url = 'http://localhost:8100/api/automobiles/';

      const response = await fetch(url);
      if (response.ok){
        const data = await response.json();
        setInventory(data.autos);
      }
    }
  useEffect(() => {
    fetchData();
  }, []);
  return(
    <>
    <h1>Current Inventory</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Colour</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {inventory?.map((car, index) => (
            <tr key={index}>
              <td>{car.vin}</td>
              <td>{car.color}</td>
              <td>{car.year}</td>
              <td>{car.model.name}</td>
              <td>{car.model.manufacturer.name}</td>
              <td>{car.sold ? "Sold" : "For Sale"}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default InventoryList;

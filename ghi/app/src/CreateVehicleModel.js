import React, { useEffect, useState } from 'react';

function NewVehicleModel() {
  const [name, setName] = useState('');
  const [picture, setPicture] = useState('');
  const [manufacturer, setManufacturer] = useState(0);
  const [listManufacturer, setListManufacturer] = useState([])

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(url);
    if (response.ok){
      const data = await response.json();
      setListManufacturer(data.manufacturers);
    }
};

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.picture_url = picture;
    data.manufacturer_id = parseInt(manufacturer);

    window.location.href='http://localhost:3000/models/'

    const makeModelURL = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(makeModelURL, fetchConfig);
    if (response.ok) {
      const knew = await response.json();
    }
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handlePictureChange = (event) => {
    const value = event.target.value;
    setPicture(value);
  };

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a vehicle model</h1>
          <form onSubmit={handleSubmit} id="create-customer-form">
            <div className="form-floating mb-3">
              <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Model name...</label>
            </div>
            <div className="form-floating mb-3">
              <input value={picture} onChange={handlePictureChange} placeholder="picture" required type="url" name="picture" id="picture" className="form-control"/>
              <label htmlFor="picture">Picture URL...</label>
            </div>
            <div className="mb-3">
              <select value={manufacturer} onChange={handleManufacturerChange} required name="listManufacturer" id="listManufacturer" className="form-select">
                <option value="">Select Manufacturer </option>
                {listManufacturer?.map(make => {
                  return (
                    <option key={make.href} value={make.id}>
                      {make.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default NewVehicleModel;

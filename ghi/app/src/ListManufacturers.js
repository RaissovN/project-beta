import { useEffect, useState } from 'react';

function ManufacturerList() {
  const [manufacturer, setManufacturer] = useState([]);

  const fetchData = async () => {
      const url = 'http://localhost:8100/api/manufacturers/';

      const response = await fetch(url);
      if (response.ok){
        const data = await response.json();
        setManufacturer(data.manufacturers);
      }
    }
  useEffect(() => {
    fetchData();
  }, []);
  return(
    <>
    <h1>Manufacturers</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturer?.map((make, index) => (
            <tr key={index}>
              <td>{make.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default ManufacturerList;

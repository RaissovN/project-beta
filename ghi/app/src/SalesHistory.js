import { useEffect, useState } from 'react';

function SalesList() {
  const [sales, setSales] = useState([]);
  const [ filter, setFilter] = useState(0);
  const [ salespeople, setSalespeople] = useState([]);

  const fetchData = async () => {
      const url = 'http://localhost:8090/api/sales/';

      const response = await fetch(url);
      if (response.ok){
        const data = await response.json();
        setSales(data.sale);
      }

      const salesPeopleUrl = 'http://localhost:8090/api/salespeople/';

      const peopleResponse = await fetch(salesPeopleUrl);
      if (peopleResponse.ok){
        const peopledata = await peopleResponse.json();
        setSalespeople(peopledata.Sales_people);
      }
    };

    const handleSalesPersonChange = (event) => {
        const value = event.target.value
        setFilter(value)
    }


  useEffect(() => {
    fetchData();
  }, []);
  return(
    <><h1>Salesperson History</h1>
    <select value={filter} onChange={handleSalesPersonChange} required name="filter" id="filter" className="form-select">
        <option value="">Select Salesperson </option>
        {salespeople.map((employee, index) => {
            return (
            <option key={index} value={employee.employee_id}>
                {employee.first_name + " " + employee.last_name}
            </option>
            );
        })}
    </select>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>VIN</th>
            <th>Customer</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales?.filter(sift => sift.salesperson.employee_id === filter).map((sale, index) => (
            <tr key={index}>
              <td>{sale.salesperson.first_name + " " + sale.salesperson.last_name}</td>
              <td>{sale.vin.vin}</td>
              <td>{sale.customer.first_name + " " + sale.customer.last_name}</td>
              <td>${sale.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default SalesList;

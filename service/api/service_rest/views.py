from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import TechnicianListEncoder, AppointmentListEncoder
from .models import Technician, AutomobileVO, Appointment


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Unable to add a technician"},
            )
            response.status_code = 404
            return response


@require_http_methods(["DELETE", "GET"])
def api_delete_technician(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Technician.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except:
            return JsonResponse({"message": "Unable to delete"})


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            employee = content.get("technician")
            technician = Technician.objects.get(employee_id=employee)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Not found"},
                status=404,
            )

        try:
            vin = AutomobileVO.objects.get(vin=content["vin"])
            if vin is not None:
                content["vip"] = True
                appointment = Appointment.objects.create(**content)
                return JsonResponse(
                    appointment,
                    encoder=AppointmentListEncoder,
                    safe=False,
                )
        except AutomobileVO.DoesNotExist:
            content["vip"] = False
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )


@require_http_methods(["DELETE"])
def api_delete_appointment(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Appointment.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except:
            return JsonResponse({"message": "Unable to delete"})


@require_http_methods(["PUT"])
def api_finish_appoinment(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = "Finished"
            appointment.save()
            return JsonResponse(
                {"message": "Appoinment is finished"},
            )
        except:
            return JsonResponse({"message": "Unable to cancel the appointment"})


@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = "Canceled"
            appointment.save()
            return JsonResponse(
                {"message": "Appoinment is canceled"},
            )
        except:
            return JsonResponse({"message": "Unable to finish the appointment"})

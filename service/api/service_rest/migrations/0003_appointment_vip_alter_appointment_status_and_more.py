# Generated by Django 4.0.3 on 2023-06-06 15:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0002_alter_technician_employee_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='vip',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='status',
            field=models.CharField(choices=[('Created', 'Created'), ('Finished', 'Finished'), ('canceled', 'Canceled')], default='Created', max_length=100),
        ),
        migrations.AlterField(
            model_name='technician',
            name='employee_id',
            field=models.PositiveIntegerField(unique=True),
        ),
    ]

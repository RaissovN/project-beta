from django.contrib import admin

# Register your models here.
from .models import AutomobileVO, Salesperson, Customer, Sale
@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = (
        "vin",
        "href",
                )

@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    list_display = (
        "first_name",
        "last_name",
        "employee_id",
                )

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = (
        "first_name",
        "last_name",
        "address",
        "phone_number",
                )

@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    list_display = (
        "vin",
        "customer",
        "salesperson",
        "price",
                )

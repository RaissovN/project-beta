from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200)
    href = models.CharField(max_length=200)

    def __str__(self):
        return self.vin

class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.TextField(max_length=500)
    phone_number = models.CharField(max_length=15)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

class Sale(models.Model):
    price = models.PositiveIntegerField()
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales_person_name",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customer_name",
        on_delete=models.CASCADE,
    )
    vin = models.ForeignKey(
        AutomobileVO,
        related_name="car_vin",
        on_delete=models.CASCADE,
    )

from django.shortcuts import render
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Customer, Sale, Salesperson
from common.json import ModelEncoder
from .encoders import SaleListEncoder, CustomerListEncoder, AutomobileVOEncoder, SalespersonListEncoder


@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        salespeople = list(Salesperson.objects.all())
        return JsonResponse(
            {"Sales_people": salespeople},
            encoder= SalespersonListEncoder,
            safe = False
        )
    else:
        content = json.loads(request.body)
        try:
             salesperson = Salesperson.objects.create(**content)
             return JsonResponse(
                salesperson,
                encoder= SalespersonListEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer ID"},
                status=404
            )
        except KeyError:
            return JsonResponse(
                {"message": "Invalid JSON payload"},
                status=400
            )



@require_http_methods(["DELETE", "GET"])
def salesperson_detail(request, pk):

    if request.method == "GET":
        sales_person = Salesperson.objects.get(id=pk)
        return JsonResponse(
            sales_person,
            encoder=SalespersonListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Salesperson.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except:
            return JsonResponse({"message": "Object not deleted"})


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customer = list(Customer.objects.all())
        return JsonResponse(
            {"customer": customer},
            encoder= CustomerListEncoder,
            safe = False
        )
    else:
        content = json.loads(request.body)
        try:
             salesperson = Customer.objects.create(**content)
             return JsonResponse(
                salesperson,
                encoder= CustomerListEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer ID"},
                status=404
            )
        except KeyError:
            return JsonResponse(
                {"message": "Invalid JSON payload"},
                status=400
            )



@require_http_methods(["DELETE", "GET"])
def customer_detail(request, pk):

    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Customer.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except:
            return JsonResponse({"message": "Object not deleted"})


@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sale = list(Sale.objects.all())
        return JsonResponse(
            {"sale": sale},
            encoder= SaleListEncoder,
            safe = False
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["vin"])
            content["vin"] = automobile
            salesperson = Salesperson.objects.get(employee_id=content["salesperson"])
            content["salesperson"] = salesperson
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder= SaleListEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile not found"},
                status=404
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson not found"},
                status=404
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer not found"},
                status=404
            )
        except KeyError:
            return JsonResponse(
                {"message": "Bad Request"},
                status=400
            )



@require_http_methods(["DELETE", "GET"])
def sale_detail(request, pk):

    if request.method == "GET":
        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Sale.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except:
            return JsonResponse({"message": "Object not deleted"})

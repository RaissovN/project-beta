
# VroomTracker 
## Revving Up Dealership Operations!

---

## Developers:

- **Randall** - Sales & Front-End Specialist
- **Nariman** - Service & Backend Maestro

---

## Overview

At the heart of `VroomTracker` is our shared vision to design a buissness application app to help dealerships streamline their operations. Nariman and I designed this business management application specifically for Independent Auto-Dealers and Dealerships. From inventory management to service center coordination and sales tracking, we've encapsulated every operational facet to drive your success.

---

## Technologies We Utilized

- **Backend**: Django (with RESTful APIs to harness 3rd party data sources)
- **Frontend**: React
- **Database**: PostgreSQL

---

## Design & Development Journey

### Service Microservice 

I established three models: Technician, AutomobileVO, and Appointment to develope the backbone of our service operations. For the Appointment model, we integrated STATUS_CHOICES, ensuring the service history page is always clear and informative. I also a robust poller, to ensure seamless operations.

We then constructed views to efficiently manage technicians and appointments, and which we tested  using Insomnia. 

### Sales Microservice

Nariman managed the intricacies of three foreign keys within a single model. He worked with three models in total, with a special Value Object (VO) dedicated to polling the Vehicle Identification Number (VIN) of our inventory vehicles.

After setting up and testing the APIs using Insomnia, we then dove into the front-end development using React. I began by setting up list and created views and connected them to the Django endpoints. 
Once we were confident in their functionality,Nariman focused on enhancing the UI's visual appeal and 
Nariman finished developing the UI for the inventory service. 

After collaborating on development, we conducted a comprehensive review to ensure everything functioned as we intended.

---

## Sneak Peek!
![VroomTracker Interface Sneak Peek](ghi/app/public/VroomTracker_Preview_Interface.png)

---

## Want to Dive In? Here's How:

1. **Clone our repository**: `git clone https://gitlab.com/RaissovN/project-beta`
2. **Hop into the directory**: `cd VroomTracker`
3. **Install the essentials**:
   - Backend: `pip install -r requirements.txt`
   - Frontend: `npm install`
4. **Get it running**:
   - Backend: `python manage.py runserver`
   - Frontend: `npm start`

*(Feel free to reach out for any additional setup queries!)*

---
